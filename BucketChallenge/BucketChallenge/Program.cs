﻿using BucketChallenge.GameEntities;
using System;

namespace BucketChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            var game = new Game();
            new GamePlayersInitials(game);

            Console.ReadLine();
        }
    }
}
