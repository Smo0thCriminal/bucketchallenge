﻿using BucketChallenge.GameEntities;
using BucketChallenge.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BucketChallenge.Players.Factory
{
    public class ThoroughCheaterPlayerCreator : PlayerFactory
    {
        public override IPlayer GenerateNewPlayer(string threadName, Game gameInstance) => new ThoroughCheaterPlayer(threadName, gameInstance);
    }
}
