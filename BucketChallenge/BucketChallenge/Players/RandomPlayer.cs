﻿using BucketChallenge.GameEntities;
using BucketChallenge.Interfaces;
using BucketChallenge.Models;
using System;
using System.Threading;

namespace BucketChallenge.Players
{
    public class RandomPlayer : BasePlayer, IPlayer
    {
        public RandomPlayer(string threadName, Game gameInstance)
        {
            _game = gameInstance;
            this._thread = new Thread(this.getAnswer);
            _thread.Name = threadName;
            _thread.Start();
        }

        public void getAnswer()
        {
            while (!_game._isFinished)
            {
                Random rand = new Random();
                var answer = rand.Next(MIN_ANSWER_VALUE, MAX_ANSWER_VALUE);
                var result = _game.ProcessAnswer(answer, _thread.Name);

                if(result > 0)
                {
                    Thread.Sleep(result);
                }
            }
        }
    }
}
