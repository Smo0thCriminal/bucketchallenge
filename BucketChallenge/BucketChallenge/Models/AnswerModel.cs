﻿namespace BucketChallenge.GameEntities
{
    public class AnswerModel
    {
        public string PlayerName { get; set; }
        public int PlayerAnswer { get; set; }

        public AnswerModel(string _playerName, int _playerAnswer)
        {
            PlayerName = _playerName;
            PlayerAnswer = _playerAnswer;
        }
    }
}
