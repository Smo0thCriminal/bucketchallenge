﻿using BucketChallenge.GameEntities;
using BucketChallenge.Interfaces;
using BucketChallenge.Models;
using System.Threading;

namespace BucketChallenge.Players
{
    public class ThoroughPlayer : BasePlayer, IPlayer
    {
        public ThoroughPlayer(string threadName, Game gameInstance)
        {
            _game = gameInstance;
            this._thread = new Thread(this.getAnswer);
            _thread.Name = threadName;
            _thread.Start();
        }

        public void getAnswer()
        {
            var answer = MIN_ANSWER_VALUE;
            while (!_game._isFinished)
            {
                var result = _game.ProcessAnswer(answer, _thread.Name);

                if (result > 0)
                {
                    answer++;
                    Thread.Sleep(result);
                }
            }
        }
    }
}
