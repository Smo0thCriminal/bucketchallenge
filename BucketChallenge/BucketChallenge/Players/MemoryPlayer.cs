﻿using BucketChallenge.GameEntities;
using BucketChallenge.Interfaces;
using BucketChallenge.Models;
using System;
using System.Collections.Generic;
using System.Threading;

namespace BucketChallenge.Players
{
    public class MemoryPlayer : BasePlayer, IPlayer
    {
        private List<int> _alreadyAnsered = new List<int>();

        public MemoryPlayer(string threadName, Game gameInstance)
        {
            _game = gameInstance;
            this._thread = new Thread(this.getAnswer);
            _thread.Name = threadName;
            _thread.Start();
        }

        public void getAnswer()
        {
            while (!_game._isFinished)
            {
                Random rand = new Random();
                int answer;
                do
                {
                    answer = rand.Next(MIN_ANSWER_VALUE, MAX_ANSWER_VALUE);
                } while (_alreadyAnsered.Contains(answer));

                _alreadyAnsered.Add(answer);
                var result = _game.ProcessAnswer(answer, _thread.Name);

                if (result > 0)
                {
                    Thread.Sleep(result);
                }
            }
        }
    }
}
