﻿using BucketChallenge.GameEntities;
using System.Threading;

namespace BucketChallenge.Models
{
    public class BasePlayer
    {
        public const int MIN_ANSWER_VALUE = 40;
        public const int MAX_ANSWER_VALUE = 140;
        public Game _game { get; set; }
        public Thread _thread { get; set; }
    }
}
