﻿using BucketChallenge.GameEntities;
using BucketChallenge.Interfaces;

namespace BucketChallenge.Players.Factory
{
    public abstract class PlayerFactory
    {
        public abstract IPlayer GenerateNewPlayer(string threadName, Game gameInstance);
    }
}
