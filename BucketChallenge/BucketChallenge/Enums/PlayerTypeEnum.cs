﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BucketChallenge.Models.Enums
{
    enum PlayerTypeEnum
    {
        RandomPlayer = 1,
        MemoryPlayer = 2,
        ThoroughPlayer = 3,
        CheaterPlayer = 4,
        ThoroughCheaterPlayer = 5
    }
}
