﻿using BucketChallenge.GameEntities;
using BucketChallenge.Interfaces;
using BucketChallenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace BucketChallenge.Players
{
    public class ThoroughCheaterPlayer : BasePlayer, IPlayer
    {
        public ThoroughCheaterPlayer(string threadName, Game gameInstance)
        {
            _game = gameInstance;
            this._thread = new Thread(this.getAnswer);
            _thread.Name = threadName;
            _thread.Start();
        }
        public void getAnswer()
        {
            while (!_game._isFinished)
            {
                var answer = 39;
                do
                {
                    answer++;
                } while (GetCheatData(answer));
                var result = _game.ProcessAnswer(answer, _thread.Name);

                if (result > 0)
                {
                    Thread.Sleep(result);
                }
            }
        }

        private bool GetCheatData(int guess)
        {
            var field = _game.GetType().GetField("_triedNumbers", BindingFlags.Instance | BindingFlags.NonPublic);
            var privateList = field.GetValue(_game) as List<AnswerModel>;
            return privateList.Select(x => x.PlayerAnswer).Contains(guess);
        }
    }
}
