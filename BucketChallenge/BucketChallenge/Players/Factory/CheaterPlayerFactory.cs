﻿
using BucketChallenge.GameEntities;
using BucketChallenge.Interfaces;

namespace BucketChallenge.Players.Factory
{
    class CheaterPlayerCreator : PlayerFactory
    {
        public override IPlayer GenerateNewPlayer(string threadName, Game gameInstance) => new CheaterPlayer(threadName, gameInstance);
    }
}
