﻿using BucketChallenge.Models.Enums;
using BucketChallenge.Players.Factory;
using System;
using System.Collections.Generic;

namespace BucketChallenge.GameEntities
{
    public class GamePlayersInitials
    {
        private readonly Dictionary<string, PlayerFactory> _factories;
        private readonly Game _gameInstance;

        public GamePlayersInitials(Game gameInstance)
        {
            _gameInstance = gameInstance;
            _factories = new Dictionary<string, PlayerFactory>();
            getPlayersInitialData();
            executePlayerFactory();
        }

        private void getPlayersInitialData()
        { 
            Console.WriteLine("Choose player type and give him a name! =)");
            Console.WriteLine($"There are several types of players: \n" +
                $"1. {PlayerTypeEnum.RandomPlayer.ToString()},\n" +
                $"2. {PlayerTypeEnum.MemoryPlayer.ToString()},\n" +
                $"3. {PlayerTypeEnum.ThoroughPlayer.ToString()},\n" +
                $"4. {PlayerTypeEnum.CheaterPlayer.ToString()},\n" +
                $"5. {PlayerTypeEnum.ThoroughCheaterPlayer.ToString()}.\n" +
                $"Enter example: 1,David");

            for (int i = 0; i < _gameInstance._playersCount; i++)
            {
                var desicion = Console.ReadLine().Split(',');
                int.TryParse(desicion[0], out var result);
                switch ((PlayerTypeEnum)result)
                {
                    case PlayerTypeEnum.RandomPlayer:
                        _factories.Add(desicion[1], new RandomPlayerFactory());
                        break;
                    case PlayerTypeEnum.MemoryPlayer:
                        _factories.Add(desicion[1], new MemoryPlayerFactory());
                        break;
                    case PlayerTypeEnum.ThoroughPlayer:
                        _factories.Add(desicion[1], new ThoroughPlayerFactory());
                        break;
                    case PlayerTypeEnum.CheaterPlayer:
                        _factories.Add(desicion[1], new CheaterPlayerCreator());
                        break;
                    case PlayerTypeEnum.ThoroughCheaterPlayer:
                        _factories.Add(desicion[1], new ThoroughCheaterPlayerCreator());
                        break;
                }
            }
            _gameInstance.StartGame();
        }

        private void executePlayerFactory()
        {
            foreach (var player in _factories)
            {
                player.Value.GenerateNewPlayer(player.Key, _gameInstance);
            }
        }
    }
}
