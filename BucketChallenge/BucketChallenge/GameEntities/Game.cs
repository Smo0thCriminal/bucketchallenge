﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace BucketChallenge.GameEntities
{
    public class Game
    {
        private const int GAME_TIME_LIMIT = 1500;

        private int _attemptsLeft { get; set; }
        public int _playersCount { get; private set; }
        private List<string> _managedThreads { get; set; }
        private Basket _basket { get; set; }
        private Stopwatch _stopWatch { get; set; }
        public bool _isFinished { get; private set; }

        private List<AnswerModel> _triedNumbers = new List<AnswerModel>();


        private object locker = new object();


        public Game()
        {
            GetInitialPlayersCount();
        }

        public void StartGame()
        {
            _stopWatch = new Stopwatch();
            _isFinished = false;
            _attemptsLeft = 100;
            _basket = new Basket();
        }

        private void GetInitialPlayersCount()
        {
            int result;
            do
            {
                Console.WriteLine("How many players do you want to play this game? 2 - 8 (default is 2 random players)");
                int.TryParse(Console.ReadLine(), out result);
            } while (!Enumerable.Range(2, 7).Contains(result));
            _playersCount = result;
        }

        public int ProcessAnswer(int answer, string playerName)
        {
            _stopWatch.Start();
            lock (locker)
            {
                _attemptsLeft--;
                //Console.WriteLine($"ANSWER: {answer}, PLAYERNAME: {Thread.CurrentThread.Name}, ATTEMTS_LEFT: {_attemptsLeft}");
                _triedNumbers.Add(new AnswerModel(playerName, answer));
                var result = _basket.BasketWeight() - answer;
                if (_attemptsLeft == 0 || answer == _basket.BasketWeight() || _stopWatch.ElapsedMilliseconds >= GAME_TIME_LIMIT)
                {
                    FinalizeGame();
                }
                return result < 0 ? result * -1 : result;
            }
        }

        private void FinalizeGame()
        {
            _stopWatch.Stop();
            _isFinished = true;
            var nearest = _triedNumbers
                .Select(x => new { Value = x, Difference = Math.Abs(x.PlayerAnswer - _basket.BasketWeight()) })
                .OrderBy(p => p.Difference)
                .First().Value;

            if (_stopWatch.ElapsedMilliseconds >= GAME_TIME_LIMIT)
            {
                Console.WriteLine($"Time is runned out! {GAME_TIME_LIMIT} ms passed! =)");
                Console.WriteLine($"The closest answer was {nearest.PlayerAnswer} by: {nearest.PlayerName}");
                return;
            }
            else if (_attemptsLeft == 0)
            {
                Console.WriteLine("No attempts left! =)");
                Console.WriteLine($"The closest answer was {nearest.PlayerAnswer} by: {nearest.PlayerName}");
                return;
            }
            Console.WriteLine($"WE HAVE A WINNER! ITS A {_triedNumbers.LastOrDefault().PlayerName} WITH ANSWER: {_triedNumbers.LastOrDefault().PlayerAnswer}");
        }
    }
}
