﻿using System;

namespace BucketChallenge.GameEntities
{
    public class Basket
    {
        private const int MIN_BASKET_VALUE = 40;
        private const int MAX_BASKET_VALUE = 140;
        private int _basketWeigth { get; set; }

        public Basket()
        {
            _basketWeigth = generateRandomWeight();
            Console.WriteLine($"Game started. The real basket weight is: {_basketWeigth}");
        }

        public int BasketWeight()
        {
            return _basketWeigth;
        }

        private int generateRandomWeight()
        {
            Random rand = new Random();
            return rand.Next(MIN_BASKET_VALUE, MAX_BASKET_VALUE);
        }
    }
}
